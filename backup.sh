#!/bin/bash
#author: wilfried Tchako
#creation date : 07/06/2021
#last modified : 10/20/2022
# description : this script will ssh inside the jenkins server and set up a crontab that will run every day by 11 pm in order to backup my jenkins server
# usage : You will run this script withnin a jenkins pipeline
# ip=$(aws cloudformation describe-stacks --stack-name jenkins2022 --query "Stacks[*].Outputs[1].OutputValue" --output text)
# ssh -o StrickHostKeyCheking=no ec2-user@$ip
sudo su
mkdir backups


echo "47 16 * * * root tar -cvf ~/backups/jenkin_backup_"$(date +%d-%m-%Y_%H-%M-%S)".tar /var/lib/jenkins 2>/dev/null" >> /etc/crontab
echo "49 16 * * * root aws s3 sync --delete ~/backups/* s3://mabanda2022" >> /etc/crontab
echo "50 16 * * * root rm -rf ~/backups/*"






