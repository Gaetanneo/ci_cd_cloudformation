#!/bin/bash
#author: wilfried Tchako
#creation date : 07/06/2021
#last modified : 10/20/2022
# description : this script will create a jenkins server and is security group
# usage : You will run this script withnin a jenkins pipeline
name=jenkins2022
test=$(aws cloudformation list-stacks --stack-status-filter CREATE_COMPLETE --query "StackSummaries" --output text)
result=$(aws cloudformation list-stacks --stack-status-filter CREATE_COMPLETE | jq -r  '.StasckSummaries[0].StackStatus') 

if [ -z  "$test" ]
then 
echo " there is no stack up "
aws cloudformation create-stack --stack-name $name --template-body file://ec2-jenkins.yaml --parameters file://value.json
aws cloudformation wait stack-create-complete --stack-name $name
elif [[ "CREATE_COMPLETE" == "$result" ]]
then 
echo " this stack already exist"

fi